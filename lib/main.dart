// ignore_for_file: unused_local_variable

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const LoginScreen(),
    );
  }
}

class LoginScreen extends StatelessWidget {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white.withOpacity(0.8),
      child: Center(
        child: Container(
          padding: const EdgeInsets.fromLTRB(30, 40, 30, 30),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'Login'.toUpperCase(),
                style:
                    const TextStyle(fontSize: 46, fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 15,
                ),
                child: CustomTextField(
                  hintText: 'Username',
                  prefixIcon: Icon(Icons.person_outline_rounded),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: 12,
                  bottom: 20,
                ),
                child: CustomTextField(
                  hintText: 'Password',
                  prefixIcon: Icon(Icons.lock_outlined),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 25, bottom: 20),
                child: GestureDetector(
                  onTap: () {},
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.deepOrange.shade500,
                        borderRadius: BorderRadius.circular(40)),
                    width: double.infinity,
                    height: 60,
                    child: Center(
                      child: Text(
                        'Login',
                        style: TextStyle(fontSize: 20, color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    'Forgot Password',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Container(
                    width: 195,
                  ),
                  Text(
                    'Help',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: FractionallySizedBox(
                    widthFactor: 0.8,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Not Registered?',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          'Create account',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.deepOrange,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class CustomTextField extends StatelessWidget {
  final String hintText;
  final Widget? prefixIcon;
  CustomTextField({Key? key, required this.hintText, this.prefixIcon})
      : super(key: key);

  InputDecoration _textFieldDecoration(String hintText) {
    final outlineInputBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(40),
      borderSide: BorderSide(
        color: Colors.deepOrange.shade200,
      ),
    );
    return InputDecoration(
      prefixIcon: prefixIcon,
      prefixIconColor: Colors.deepOrange.shade300,
      filled: true,
      fillColor: Colors.deepOrange.shade100,
      hintText: hintText,
      hintStyle: TextStyle(
        color: Colors.grey.shade600,
        fontWeight: FontWeight.bold,
        fontSize: 14,
      ),
      contentPadding: EdgeInsets.all(20),
      constraints: BoxConstraints(
        maxHeight: 100,
      ),
      enabledBorder: outlineInputBorder,
      focusedBorder: outlineInputBorder,
      border: outlineInputBorder,
    );
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: _textFieldDecoration(hintText),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildAppBar(),
      body: _buildBody(),
      drawer: Container(
        color: Colors.deepOrange,
        width: 250,
      ),
      bottomNavigationBar: _bottomNavigationBar(),
    );
  }

  BottomNavigationBar _bottomNavigationBar() {
    return BottomNavigationBar(
      backgroundColor: Colors.white,
      currentIndex: selectedIndex,
      selectedItemColor: Colors.blue,
      unselectedItemColor: Colors.black,
      iconSize: 30,
      selectedLabelStyle: const TextStyle(
        fontWeight: FontWeight.bold,
      ),
      onTap: (index) {
        setState(() {
          selectedIndex = index;
        });
      },
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.message_outlined),
          label: 'Tin nhắn',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.contact_page_outlined,
            semanticLabel: 'Contacts',
            textDirection: TextDirection.ltr,
          ),
          label: 'Danh bạ',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.apps_sharp,
              semanticLabel: 'Khám phá', textDirection: TextDirection.ltr),
          label: 'Khám phá',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.access_time,
            semanticLabel: 'Nhật kí',
            textDirection: TextDirection.ltr,
          ),
          label: 'Nhật kí',
        ),
        BottomNavigationBarItem(
          icon: Icon(
            Icons.person_outlined,
            semanticLabel: 'Cá nhân',
            textDirection: TextDirection.ltr,
          ),
          label: 'Cá nhân',
        ),
      ],
    );
  }

  Container _buildBody() {
    return Container(
      color: Color.fromARGB(255, 217, 220, 223),
      margin: const EdgeInsets.all(
        10,
      ),
      child: Column(
        children: [
          Text("Flutter"),
          Row(
            children: [
              Text(
                "Hello, World!",
              ),
            ],
          ),
        ],
      ),
    );
  }

  AppBar _buildAppBar() {
    return AppBar(
      backgroundColor: Color.fromARGB(255, 0, 140, 255),
      title: const Text(
        'Tìm kiếm',
        style: TextStyle(
          color: Colors.white,
          fontSize: 15,
          fontWeight: FontWeight.w100,
          letterSpacing: 0.5,
        ),
      ),
      leading: IconButton(
        onPressed: () {
          print('you are pressing on Search');
        },
        icon: const Icon(
          Icons.search_outlined,
          color: Colors.white,
        ),
      ),
      actions: [
        IconButton(
          onPressed: () {
            print('you are pressing on add');
          },
          icon: const Icon(
            Icons.qr_code_scanner,
            color: Colors.white,
          ),
        ),
        IconButton(
          onPressed: () {
            print('you are pressing on add');
          },
          icon: const Icon(
            Icons.add,
            color: Colors.white,
            size: 35,
          ),
        ),
      ],
    );
  }
}


// class MyHomePage extends StatefulWidget {
//   const MyHomePage({super.key, required this.title});
//   final String title;

//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
//   int _counter = 0;

//   void _incrementCounter() {
//     setState(() {
//       _counter++;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.deepOrange,
//         title: const Text('Home',
//             style: TextStyle(
//                 color: Colors.white,
//                 fontSize: 35,
//                 fontWeight: FontWeight.bold)),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             const Text(
//               'You have pushed the button this many times:',
//             ),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.headlineMedium,
//             ),
//           ],
//         ),
//       ),
//       floatingActionButton: FloatingActionButton(
//         onPressed: _incrementCounter,
//         tooltip: 'Increment',
//         child: const Icon(Icons.add),
//       ), // This trailing comma makes auto-formatting nicer for build methods.
//     );
//   }
// }
